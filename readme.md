# Android flutter App

![android dev desk](android_dev_desk.jpg)![dart](dart.png)![flutter](flutter.png)

A medium android flutter app to develop some best practices for migrating to flutter from native classic 
android and ios code app development.  This migration strategy is called the free-falling strategy 
obviously after Tom Petty's song and involves throwing away the legacy app code and taking the 
pain full upfront and rebuild the app from scratch using the flutter cross platform 
framework.

## Strategies

Using the Flux App Architecture pattern via Google's unoffical Flux package, see:

[Flutter Flux Implementation](https://github.com/google/flutter_flux)

Further app architecture tuning will include using Finite State Machines(FSM) for the 
final leg in managing state within the app.

Also using the platform widgets framework and strategy for delivering Material widgets 
for android and delivering Cupertino widgets for iOS. See the platform widgets package at:


[Flutter Platform widgets](https://github.com/aqwert/flutter_platform_widgets)

Reactive wise instead of using RX extensions I am using the reactive components in Dart the same way 
Google is using them as its less complex and yet stil as powerful as it needs to be.

## AssetLog

3-11 Google Noto fonts are licensed SIL open font license 1.1 and 
Noto is a trademark owned by Google

## Screenshots

![screenone](screenshot-2019-03-11_small.png)

## Videos



# Credits

Fred Grott

For inquiries from funded startups, cofounders use my email address of 

fred   DOT

grott   AT

gmail   DOT

com


# License

Copyright 20019 Fred Grott

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.